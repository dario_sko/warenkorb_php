<?php
require_once "models/Buch.php";
require_once "models/Warenkorb.php";
require_once "models/BuchDTO.php";
$korb = new Warenkorb();
$buecher = Buch::getAll();


$errors = [];
$buchId = "";
$menge = "";

if (isset($_POST['submit'])) {
    $buchId = $_POST['buchId'] ?? "";
    $menge = $_POST['menge'] ?? "";

    if (Buch::getPerId($_POST['buchId'])) {
        $korb->hinzufuegen(Buch::getPerId($_POST['buchId']), $_POST['menge']);
    }
}
?>
<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Bootstrap ICONS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="shortcut icon" href="#">
    <title>Bücher-Shop</title>

</head>
<body>
<div class="container">
    <h3 class="text-center">Bücher Übersicht</h3>
    <div class="row">
        <div class="col text-center">
            <a href="korb.php"><i
                        class="bi-cart-fill text-black h5 text-decoration-none">Warenkorb</i>
                <p class="text-black fw-bold"><?= " " . $korb->getCountKorb(); ?></p>
            </a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-8">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Titel</th>
                        <th class="text-end">Preis</th>
                        <th>Menge</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if (!empty(Buch::$errors) || !empty(Warenkorb::$errors)) {
                        $errors[] = Buch::$errors;
                        $errors[] = Warenkorb::$errors;

                        if (!empty($errors)) {
                            echo "<div class='alert alert-danger'><ul>";
                            foreach ($errors as $key) {
                                foreach ($key as $message)
                                    echo "<li>" . $message . "</li>";
                            }
                            echo "</ul></div>";
                        }
                    }
                    if (!empty($buecher)) {
                        foreach ($buecher as $buch) {
                            $id = $buch->getId();
                            echo "<form method='post' action='index.php'>
                              <tr>
                              <td>" . $buch->getTitle() . "</td>
                              <td class='text-end'>" . $buch->getPrice() . " € </td>
                              <td>";

                            echo "<select id='menge' name='menge' required class='form-select-sm' style='width: 50px'>
                                  <option hidden value=''>-</option>";
                            for ($i = 1; $i <= $buch->getStock(); $i++) {
                                echo "<option value='$i'>" . $i . "</option>";
                            }
                            echo
                            "<input type='hidden' id='buchId' name='buchId' value='$id'>
                              </td>
                              <td>
                              <button id='addCart' class='btn btn-success' type='submit' name='submit'><i class='bi-cart-plus-fill'></i></input>
                              </td>
                              </tr>
                              </form>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
