<?php
require_once "models/Buch.php";
require_once "models/BuchDTO.php";
require_once "models/Warenkorb.php";

$korb = new Warenkorb();
$inhalt = $korb->getKorb();

if (isset($_POST['submit'])) {
    $korb->loeschen($_POST['buch']);
    header("Refresh:0");
}
?>
<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Bootstrap ICONS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="shortcut icon" href="#">
    <title>Bücher-Shop</title>

</head>
<body>
<div class="container">
    <h3 class="text-center">Warenkorb Übersicht</h3>
    <div class="row">
        <div class="col text-center">
            <a href="index.php"><i class="bi-cart-fill text-black h5 text-decoration-none">Shop</i></a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-8">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Titel</th>
                        <th class="text-end">Preis</th>
                        <th>Menge</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if (!empty($inhalt)) {
                        foreach ($inhalt as $buch) {
                            $id = intval($buch->getBuch()->getId());
                            echo "<tr>
                              <td hidden>" . $buch->getBuch()->getId() . "</td>
                              <td>" . $buch->getBuch()->getTitle() . "</td>
                              <td class='text-end'>" . $buch->getBuch()->getPrice() . " € </td>
                              <td>" . $buch->getQuantity() . "</td>
                              <form method='post' action='korb.php' id='form_remove'>
                              <input type='hidden' id='buch' name='buch' value='$id'>
                              <td><button id='removeCart' class='btn btn-success' type='submit' name='submit'><i class='bi-cart-x-fill'></i></button></td>
                              </form>
                              </tr>";
                        }
                        echo "<tr>
                              <td class='fw-bold'>Gesamtsumme</td>
                              <td class='text-end fw-bold'>" . $korb->sumKorb() . " € </td></tr>";
                    } else {
                        echo "<tr>
                              <td colspan='4' class='text-center'><p>Keine Bücher vorhanden</p></td>
                              </tr>";
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
