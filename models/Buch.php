<?php

class Buch
{
    private static $books = [];
    public static $errors = [];

    private $id;
    private $title;
    private $price;
    private $stock;

    function __construct($id, $title, $price, $stock)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->stock = $stock;
    }

    public static function getAll()
    {
        if (file_exists("bookdata.json")) {
            $jsonData = file_get_contents("bookdata.json");
            $jsonDecoded = json_decode($jsonData, true);

            foreach ($jsonDecoded as $key) {
                $buch = new Buch($key["id"], $key['title'], $key['price'], $key['stock']);
                Buch::$books[] = $buch;
            }
            return Buch::$books;
        } else {
            Buch::$errors["notFound"] = "Die Datei ist nicht vorhanden!";
            return false;
        }
    }

    public static function getPerId($id)
    {
        $buch = Buch::getAll();

        if (!empty($buch)) {
            foreach ($buch as $buchId) {
                if ($buchId->getId() == $id) {
                    return $buchId;
                }
            }
            Buch::$errors['bookIdNotFound'] = "Das Buch mit der ID " . $id . " konnte nicht gefunden werden!";
            return false;
        } else {
            Buch::$errors['noBooksfound'] = "Keine Bücher vorhanden!";
            return false;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param double $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }


}

?>
