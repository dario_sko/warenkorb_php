<?php

class BuchDTO
{
    private $buch;
    private $quantity;

    public function __construct(Buch $buch, $quantity)
    {
        $this->buch = $buch;
        $this->quantity = $quantity;
    }

    /**
     * @return Buch
     */
    public function getBuch(): Buch
    {
        return $this->buch;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


}