<?php

class Warenkorb
{
    public static $errors = [];

    private $korb = [];

    public function __construct()
    {
        $this->ladeCookie();
    }

    private function ladeCookie()
    {
        if (isset($_COOKIE["warenkorb"])) {
            $warenkorb = $_COOKIE["warenkorb"];
            $s = unserialize($warenkorb);
            $this->korb = $s;
        }
    }

    private function speicherCookie()
    {
        $s = serialize($this->korb);
        setcookie("warenkorb", $s, time() + 3600, "/");

    }

    public function hinzufuegen(Buch $book, $counter)
    {
        if ($this->validate($book, $counter)) {
            $addBook = new BuchDTO($book, $counter);
            $itemVorhanden = false;
            foreach ($this->korb as $item) {
                // Überprüfen ob Buch schon im Warenkorb vorhanden, falls ja wird die neue Menge hinzugefügt
                if ($item->getBuch()->getId() == $addBook->getBuch()->getId()) {
                    if ($item->getQuantity() + $addBook->getQuantity() <= $item->getBuch()->getStock()) {
                        $item->setQuantity($item->getQuantity() + $addBook->getQuantity());
                    } else {
                        Warenkorb::$errors['quantityMax'] = "Maximale Menge überschritten. Maximale Menge wird hinzugefügt!";
                        $item->setQuantity($item->getBuch()->getStock());
                    }
                    $itemVorhanden = true;
                    break;
                }
            }
            // Wenn Buch noch nicht im Warenkorb vorhanden wird es neu hinzugefügt
            if (!$itemVorhanden) {
                $this->korb[] = $addBook;
            }

            $this->speicherCookie();

        }
    }

    public function loeschen($id)
    {
        foreach ($this->korb as $item) {
            if ($item->getBuch()->getId() == $id) {
                $index = array_search($item, $this->korb);
                unset($this->korb[$index]);
                break;
            }
        }
        $this->speicherCookie();
    }

    public function getKorb()
    {
        return $this->korb;
    }

    public function sumKorb()
    {
        $buecher = Buch::getAll();
        $sum = 0;
        $price = 0;
        foreach ($this->korb as $buchDTO) {
            foreach ($buecher as $buch) {
                if ($buchDTO->getBuch()->getId() == $buch->getId()) {
                    $price = $buch->getPrice();
                }
            }
            $sum += $price * $buchDTO->getQuantity();
        }
        return $sum;
    }

    public function getCountKorb()
    {
        $result = 0;
        if (!empty($this->korb)) {
            foreach ($this->korb as $buch) {
                $result += $buch->getQuantity();
            }
        }
        return $result;
    }

    public function validateBuchId($buchId)
    {
        if (empty($buchId)) {
            Warenkorb::$errors['bookIdFail'] = "Buch ID ist ungültig!";
            return false;
        } else {
            return true;
        }
    }

    public function validateMenge($menge)
    {
        if ($menge <= 0 && !is_numeric($menge)) {
            Warenkorb::$errors['quantityError'] = "Es wurde eine ungültige Menge ausgewählt";
            return false;
        } else {
            return true;
        }
    }

    public function validate($buch, $menge)
    {
        return $this->validateBuchId($buch) & $this->validateMenge($menge);
    }

}

?>
